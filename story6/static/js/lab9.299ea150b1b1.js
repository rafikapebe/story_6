var counter = 0;
        function changeStar(id){
            var star = $('#'+id).html();
            if(star.includes("gray")) {
                counter++;
                $('#'+id).html("<i class='fa fa-heart' style = 'color : red'></i>");
                $("#statusBuku").html("<i class='fa fa-heart'style = 'color : red'></i> " +counter + " ");
            }
            else{
                counter--;
                $('#'+id).html("<i class='fa fa-heart' style = 'color : gray'></i>");
                $("#statusBuku").html("<i class='fa fa-heart'style = 'color : red'></i> " +counter + " ");
            }   
        }

$(document).ready(function(){
	$.ajax({
		url: "/fileJson",
		success: function(result){
			result = result.items;
			var header = "<thead><tr><th>Title</th> <th>Author</th> <th>Cover</th> <th>Description</th> <th>Publisher</th> <th>Published Date</th> <th></th> </tr><head>";
			$("#head-content").append(header);
			$("#head-content").append("<tbody>");

			for(i=0; i<result.length; i++){
				var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>"+"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>"+"</td><td>" + result[i].volumeInfo.description +"</td><td>" + result[i].volumeInfo.publisher +"</td><td>"+ result[i].volumeInfo.publishedDate +"</td><td>" +"<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'changeStar(" +"\""+result[i].id+"\""+")'><i class='fa fa-heart'style = 'color : gray'></i></button>"+"</td></tr>";
				$("#head-content").append(tmp);
			}
			$("#head-content").append("</tbody>");
		}
	});
});
