from django.test import TestCase, Client
from django.urls import resolve
from .forms import SearchForm
from .views import *

# Create your tests here.
class bookUnitTest(TestCase):
    def test_book_url_exist(self):
        response = Client().get('/coba/')
        self.assertEqual(response.status_code, 404)

    # def test_book_use_search_book_function(self):
    #     found = resolve('/hehe/')
    #     self.assertEqual(found.func, search_book)
    # def test_book_has_form(self):
    #     form = SearchForm()
    #     self.assertIn('class="form-control', form.as_p())
    # def test_form_validation_for_blank_items(self):
    #     form = SearchForm(data={'title': ''})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(
    #         form.errors['title'],
    #         ["This field is required."]
    #     )
    # def test_search_book_post_success_and_render_the_result(self):
    #     test = 'quilting'
    #     response_post = Client().post('/hehe/', {'title': test})
    #     self.assertEqual(response_post.status_code, 404)
    #     html_response = response_post.content.decode('utf8')
    #     self.assertIn(test, html_response)
