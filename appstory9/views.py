from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from .forms import SearchForm
from .models import FavBook
import requests
import json

# Create your views here.
def search_book(request):
    response = {}
    response['form'] = SearchForm()
    response['fav'] = FavBook.objects.all().count
    response['data'] = None
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            search = cd['title']
            URL = "https://www.googleapis.com/books/v1/volumes?q=" + search
            get_json = requests.get(URL).json()
            json_dict = json.dumps(get_json)
            response['data'] = json_dict
            response['fav'] = FavBook.objects.all().count
        return render(request, "searchbook.html", response)
    else:
        return render(request, "searchbook.html", response)

    if request.user.is_authenticated:
                if 'counter' not in request.session:
                    request.session['counter'] = 0
                count = request.session['counter']
                response['counter'] = count
                if 'nama' not in request.session:
                    request.session['nama'] = request.user.username
                if 'email' not in request.session:
                    request.session['email'] = request.user.email
        return render(request, "searchbook.html", response)
    
    else:
        if request.user.is_authenticated:
            print(request.user.username)
            if 'nama' not in request.session:
                    request.session['nama'] = request.user.username
            if 'email' not in request.session:
                request.session['email'] = request.user.email
            if 'counter' not in request.session:
                request.session['counter'] = 0
            count = request.session['counter']
            response['counter'] = count
            print(dict(request.session))
        return render(request, "searchbook.html", response)


def add_fav(request):
if request.user.is_authenticated:
    # judul = request.GET.get('judul', None)
    # link = request.GET.get('link', None)
    # img = request.GET.get('img', None)
    # author = request.GET.get('author', None)
    # fav = FavBook(title=judul, img=img, link=link, author=author)
    # fav.save()
    print(dict(request.session))
    request.session['counter'] = request.session['counter'] + 1
    print(dict(request.session))
else:
    pass
    # data = FavBook.objects.all()
    # data_json = serializers.serialize('json', data)
return HttpResponse(data_json, content_type='aplication/json')

def del_fav(request):
if request.user.is_authenticated:
    # judul = request.GET.get('judul', None)
    # del_data = FavBook.objects.filter(title=judul)
    # del_data.delete()
    # data = FavBook.objects.all()
    # data_json = serializers.serialize('json', data)
    request.session['counter'] = request.session['counter'] - 1
    print(dict(request.session))
    return HttpResponse(data_json, content_type='aplication/json')
    
def my_fav(request):
    response = {}
    response['data'] = FavBook.objects.all()
    return render(request, "fav.html", response)

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/books')

