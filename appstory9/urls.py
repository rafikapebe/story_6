from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'appstory9'

urlpatterns = [
	path('', views.search_book, name="searchbook"),
	path('add-fav', views.add_fav, name="add-fav"),
	path('del-fav', views.add_fav, name="del-fav"),
	path('my-fav', views.my_fav, name="fav"),
	path('logout', views.logout, name="logout"),
]
