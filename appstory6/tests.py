from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage
from .models import Status
from .forms import Landing_Page

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

    
# Create your tests here.
class landingPageTest(TestCase):

	# buat setup database pertama kali, Status.objects create blablabla

	# mengetes url nya
	def url_is_exist(self):
		
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	# mengetes models
	def using_status_model(self):
		# Membuat status baru
		Status.objects.create(teks='Whats up today?')
		# mengetes jumlah object
		jumlahObjek = Status.objects.all().count()
		self.assertEqual(jumlahObjek, 1)

	def test_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('', {'status': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_post_error_and_render_the_result(self):
		
		test = 'Anonymous'
		response_post = Client().post('', {'status': ''})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)
	
	def test_get(self):
		
		Status.objects.create(status='Rafika was here')
		test = Status.objects.first().status
		response= Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_profile_page_has_name(self):
		response = Client().get('')
		profile_page_content = "Rafika Balqis's Profile"
		html_response = response.content.decode('utf8')
		self.assertIn(profile_page_content, html_response)

	def test_lab10_subscribe_url(self):
		response = Client().get('/subscribe')
		self.assertEqual(response.status_code, 200)



class Lab7FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab7FunctionalTest, self).tearDown()

	def test_input_todo(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://127.0.0.1:8000/')
		# find the form element
		status = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('submit')

		# Fill the form with data
		time.sleep(4)
		status.send_keys('Coba Coba')

		# submitting the form
		submit.send_keys(Keys.RETURN)
		time.sleep(4)

	def test_layout_profile(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		header_text = selenium.find_elements_by_tag_name('h1')
		self.assertIn("Rafika Balqis's Profile", header_text[-1].text)

		p_text = selenium.find_elements_by_tag_name('p')
		self.assertIn('Nama Lengkap: Rafika Putri Balqis\nNPM: 1706023681\nInstitusi: Universitas Indonesia\nUmur: 19\nHobi: Jalan - Jalan\nCita - Cita: bahagia deh pokoknya\nDeskripsi Diri: saya orangnya ramah baik hati dan tidak sombong\nSocial Media: instagram twitter', p_text[-1].text)
		time.sleep(3)

	def test_css_is_correct(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		class_header_css = selenium.find_element_by_css_selector('h1.header') 
		class_foto_css = selenium.find_element_by_css_selector('img.foto')
		time.sleep(3)

	 
