from django.urls import path
from django.contrib import admin
from . import views
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *

urlpatterns = [
	path('', homepage, name="landingpage"),
	path('delete_new', views.delete_new, name='delete_new'),
	path('subscribe', views.subscribe, name="subscribe"),
    path('validate', views.checkValid, name='validate'),
    path('success', views.success, name="success"),
    path('allsubs', getallsubs, name="allsubs"),
    path('subscribers', subspage, name="subscribers"),
    path('delsub', delsub),


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)