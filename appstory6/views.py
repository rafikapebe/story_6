from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Status, Subscribe
from .forms import Landing_Page, SubscriberForm
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

def homepage(request):
	form = Landing_Page(request.POST or None)
	response = {}
	# handle post request saat ngesubmit inputan dr form
	if(request.method == "POST"):  
		if(form.is_valid()):
			status = request.POST.get("status")
			Status.objects.create(
				status=status,
			)

			return redirect('/landingpage')
		else:
			return render(request, 'landingpage.html', response)

	# pas akses urlnya
	elif(request.method == "GET"):
		lstStatus = Status.objects.all() 
		response['form'] = form
		response['lstStatus'] = lstStatus
		return render(request, 'landingpage.html', response)

def delete_new(request):
    Status.objects.all().delete()
    return redirect('/')

def subscribe(request):
	response = {}
	response["forms"] = SubscriberForm()
	return render(request, "subscribe.html", response)

@csrf_exempt
def checkValid(request):
	email = request.POST.get("email")
	data = {'not_valid': Subscribe.objects.filter(email__iexact=email).exists()}
	return JsonResponse(data)

def success(request):
	submit_form = SubscriberForm(request.POST or None)
	if(submit_form.is_valid()):
		cd = submit_form.cleaned_data
		newSubscriber = Subscribe(name=cd['name'], password=cd['password'], email=cd['email'])
		newSubscriber.save()
		data = {'name': cd['name'], 'password': cd['password'], 'email':cd['email']}
	return JsonResponse(data)

def getallsubs(request):
    data_json = serializers.serialize('json', Subscriber.objects.all())
    return HttpResponse(data_json, content_type='aplication/json')

def subspage(request):
    return render(request, "subscribe.html") 

@csrf_exempt
def delsub(request):
    if 'email' in request.POST:
        print('mantap')
        email = request.POST['email']
        lele = Subscriber.objects.filter(email=email)
        lele.delete()
        return HttpResponseRedirect("/allsubs")
    else:
        print('ngapa DAH')
        return HttpResponseRedirect("/allsubs")
